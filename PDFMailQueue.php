<?php
class PDFMailQueue{
	public static $stackSrc = "stack.json";
	public static function push($mail,$pdfPrint,$priority){
		$stack = json_decode(@file_get_contents(self::$stackSrc),1);
		if(empty($stack)){$stack=[];}
		array_push($stack,["mail"=>$mail,"pdfPrint"=>$pdfPrint,"priority"=>$priority]);
		//sort here later
		file_put_contents(self::$stackSrc,json_encode($stack));
	}
	public static function pop(){
		$stack = json_decode(@file_get_contents(self::$stackSrc),1);
		if(empty($stack)){$stack=[];}
		$ret = array_pop($stack);
		file_put_contents(self::$stackSrc,json_encode($stack));
		return $ret;
	}
}
?>