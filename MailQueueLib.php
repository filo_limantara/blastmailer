<?php
class MailQueue{
	private $isAuthd;
	private $config;
	public static $stackSrc = "stack.json";	
	public function __construct($username,$password){
		$this->isAuthd = false;
		$this->config = NULL;
		//login process
		$allConfig = json_decode(file_get_contents("MailQueueConfig.json"),1);
		if(empty($allConfig)){throw new Exception('invalid MailQueue config');}
		foreach($allConfig as $idx=>$obj){
			if($obj['username']==$username){
				if($obj['password']==$password){
					$this->isAuthd = true;
					$this->config = $obj;
					unset($this->config['username']);
					unset($this->config['password']);
				}
			}
		}
	}
	public function isAuthenticated(){return $this->isAuthd;}
	public function getConfig(){return $this->config;}
	//---
	public static function push($mail,$files,$priority,$validuntil){
		$stack = json_decode(@file_get_contents(self::$stackSrc),1);
		if(empty($stack)){$stack=[];}
		array_push($stack,["mail"=>$mail,"files"=>$files,"priority"=>$priority, "validuntil"=>$validuntil]);
		//sort here later
		file_put_contents(self::$stackSrc,json_encode($stack));
	}
	public static function pop(){
		$stack = json_decode(@file_get_contents(self::$stackSrc),1);
		if(empty($stack)){$stack=[];}
		$ret = array_pop($stack);
		file_put_contents(self::$stackSrc,json_encode($stack));
		return $ret;
	}	
}
?>