<?php
function easyFileUploadHandle($paramNames,$target_dir){//reads file and return a filename and path
	$ret = [];
	if(!empty($paramNames)){
		foreach($paramNames as $idx=>$paramName){
			if(!empty($_FILES[$paramName])){
				$currParam = $_FILES[$paramName];
				foreach($currParam['name'] as $key=>$filename){
					if(!is_int($key)){
						$valid = preg_match('=^[^/?*;:{}\\\\]+\.[^/?*;:{}\\\\]+$=', $key);
						if($valid){$filename = $key;}
						else{throw new Exception('invalid filename '.$key);}
					}
					//create new name
					$target_file = $target_dir . uniqid();
					//check for duplicate
					while(file_exists($target_file)){
						$target_file = $target_dir . uniqid();
					}
					//move to safe place
					move_uploaded_file($currParam["tmp_name"][$key],$target_file);
					$ret[$filename]=$target_file;
				}
			}
		}
	}
	return $ret;
}

?>