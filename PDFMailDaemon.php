<?php
require("PDFMailQueue.php");
require("SimpleMailerLib.php");
$pdfFolder = "pdfs";
$pdfServer = "http://localhost/BlastMailer/PDFServer.php";

$idleMsg = "idle";
while(1){
	try{
		$data = PDFMailQueue::pop();
		if(empty($data)){echo("$idleMsg.");sleep(2);$idleMsg="";continue;}
		$idleMsg = "idle";
		//receive requests
		//$mail = json_decode(@$_POST['mail'],1);//ok
		//$pdfPrint = json_decode(@$_POST['pdfPrint'],1);//ok
		//$files = [];//json_decode(@$_POST['files'],1);//ok
		$mail = @$data['mail'];//ok
		$pdfPrint = @$data['pdfPrint'];//ok

		//open connection
		$ch = curl_init();
		foreach($pdfPrint as $filename=>$printData){
			//prepare data to POST
			$fields = array(//bad, too casuistic!
				'form_code' => json_encode($printData['form_code']),
				'lang' => json_encode($printData['lang']),
				'data' => json_encode($printData['data'])
			);
			
			//url-ify the data for the POST
			$fields_string = "";
			foreach($fields as $key=>$value) {
				$fields_string .= $key.'='.$value.'&';
			}
			rtrim($fields_string, '&');

			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $pdfServer);
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			//send print request to server and get file requested
			$result = curl_exec($ch);
			
			//add generated pdf file path to file
			$filePath = $pdfFolder."/".$filename;
			
			//save file requested to a certain folder
			file_put_contents($filePath,$result);
			$files[$filename] = $filePath;
		}
		//close connection
		curl_close($ch);

		//send mail
		SimpleMailerLib::send($mail,$files);
	}catch(Exception $e){}	
}
?>