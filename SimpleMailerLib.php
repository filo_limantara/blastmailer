<?php
/*
 * A simpler function to use PHPMailer from given JSON
 */
require 'PHPMailer-master/PHPMailerAutoload.php';
/*set_error_handler('exceptions_error_handler');

function exceptions_error_handler($severity, $message, $filename, $lineno) {
  if (error_reporting() == 0) {
	return;
  }
  if (error_reporting() & $severity) {
	throw new ErrorException($message, 0, $severity, $filename, $lineno);
  }
}
*/
class SimpleMailerLib{
	public static function autofill_default($params){
		return $params;
	}
	public static function get_required($params){
		$required = [
			//"Message",
			"From_Email",
			"To_Email",
			"Subject",
			"test_type"
		];
		if(empty($params['Message']) && empty($params['Text'])){
			$required[] = "Message";
		}
		if(@$params['test_type']=='smtp'){
			$required[] = "smtp_server";
			$required[] = "smtp_port";
			$required[] = "smtp_secure";
			$required[] = "smtp_debug";
			if(@$params['smtp_authenticate']==1){
				$required[] = "authenticate_password";
				$required[] = "authenticate_username";
			}
		}
		return $required;
	}
	public static function validate_required($params,$required){
		$msg = [];
		foreach($required as $i=>$field){
			if(!isset($params[$field])){
				$msg[] = "$field is required.";
			}
		}
		return $msg;//[]=$params;
	}	
	public static function send($request,$files,$deleteFilesFlag){
		try{
			//$files = (array)json_decode(@$_POST['files']);
			//$request = (array)json_decode(@$_POST['mail']);
			
			$request = self::autofill_default($request);
			$required = self::get_required($request);
			$errMsg = self::validate_required($request,$required);
			if(!empty($errMsg)){//return error message!
				print_r(json_encode([
					"error"=>2,
					"message"=>$errMsg
				]));
				die();
			}
			
			// storing all status output from the script to be shown to the user later
			$results_messages = array();

			// $example_code represents the "final code" that we're using, and will
			// be shown to the user at the end.
			$example_code = "\nrequire_once '../PHPMailerAutoload.php';";
			$example_code .= "\n\n\$results_messages = array();";

			$mail = new PHPMailer(true);  //PHPMailer instance with exceptions enabled
			$mail->CharSet = 'utf-8';
			ini_set('default_charset', 'UTF-8');
			$mail->Debugoutput = 'html';
			$example_code .= "\n\n\$mail = new PHPMailer(true);";
			$example_code .= "\n\$mail->CharSet = 'utf-8';";
			$example_code .= "\nini_set('default_charset', 'UTF-8');";

			//class phpmailerException extends phpmailerException{}

			$example_code .= "\n\nclass phpmailerException extends phpmailerException {}";
			$example_code .= "\n\ntry {";

			//print_r($request);

			try {
				$to = $request['To_Email'];
				if (!PHPMailer::validateAddress($to)) {
					throw new phpmailerException("Email address " . $to . " is invalid -- aborting!");
				}

				$example_code .= "\n\$to = '{$request['To_Email']}';";
				$example_code .= "\nif(!PHPMailer::validateAddress(\$to)) {";
				$example_code .= "\n  throw new phpmailerException(\"Email address \" . " .
					"\$to . \" is invalid -- aborting!\");";
				$example_code .= "\n}";

				switch ($request['test_type']) {
					case 'smtp':
						$mail->isSMTP(); // telling the class to use SMTP
						$mail->SMTPDebug = (integer)$request['smtp_debug'];
						$mail->Host = $request['smtp_server']; // SMTP server
						$mail->Port = (integer)$request['smtp_port']; // set the SMTP port
						if ($request['smtp_secure']) {
							$mail->SMTPSecure = strtolower($request['smtp_secure']);
						}
						$mail->SMTPAuth = array_key_exists('smtp_authenticate', $request); // enable SMTP authentication?
						if (array_key_exists('smtp_authenticate', $request)) {
							$mail->Username = $request['authenticate_username']; // SMTP account username
							$mail->Password = $request['authenticate_password']; // SMTP account password
						}

						$example_code .= "\n\$mail->isSMTP();";
						$example_code .= "\n\$mail->SMTPDebug  = " . $request['smtp_debug'] . ";";
						$example_code .= "\n\$mail->Host       = \"" . $request['smtp_server'] . "\";";
						$example_code .= "\n\$mail->Port       = \"" . $request['smtp_port'] . "\";";
						$example_code .= "\n\$mail->SMTPSecure = \"" . strtolower($request['smtp_secure']) . "\";";
						$example_code .= "\n\$mail->SMTPAuth   = " . (array_key_exists(
							'smtp_authenticate',
							$request
						) ? 'true' : 'false') . ";";
						if (array_key_exists('smtp_authenticate', $request)) {
							$example_code .= "\n\$mail->Username   = \"" . $request['authenticate_username'] . "\";";
							$example_code .= "\n\$mail->Password   = \"" . $request['authenticate_password'] . "\";";
						}
						break;
					case 'mail':
						$mail->isMail(); // telling the class to use PHP's mail()
						$example_code .= "\n\$mail->isMail();";
						break;
					case 'sendmail':
						$mail->isSendmail(); // telling the class to use Sendmail
						$example_code .= "\n\$mail->isSendmail();";
						break;
					case 'qmail':
						$mail->isQmail(); // telling the class to use Qmail
						$example_code .= "\n\$mail->isQmail();";
						break;
					default:
						throw new phpmailerException('Invalid test_type provided');
				}

				try {
					if (@$request['From_Name'] != '') {
						$mail->addReplyTo($request['From_Email'], $request['From_Name']);
						$mail->setFrom($request['From_Email'], $request['From_Name']);

						$example_code .= "\n\$mail->addReplyTo(\"" .
							$request['From_Email'] . "\", \"" . $request['From_Name'] . "\");";
						$example_code .= "\n\$mail->setFrom(\"" .
							$request['From_Email'] . "\", \"" . $request['From_Name'] . "\");";
					} else {
						$mail->addReplyTo($request['From_Email'], $request['From_Email']);
						$mail->setFrom($request['From_Email'], $request['From_Email']);

						$example_code .= "\n\$mail->addReplyTo(\"" . $request['From_Email'] . "\");";
						$example_code .= "\n\$mail->setFrom(\"" .
							$request['From_Email'] . "\", \"" . $request['From_Email'] . "\");";
					}

					if (@$request['To_Name'] != '') {
						$mail->addAddress($to, $request['To_Name']);
						$example_code .= "\n\$mail->addAddress(\"$to\", \"" . $request['To_Name'] . "\");";
					} else {
						$mail->addAddress($to);
						$example_code .= "\n\$mail->addAddress(\"$to\");";
					}

					if (@$request['bcc_Email'] != '') {
						$nameBCC = [];
						if(@$request['bcc_Name'] != ''){
							$nameBCC = explode(" ", $request['bcc_Name']);
						}
						$indiBCC = explode(" ", $request['bcc_Email']);
						foreach ($indiBCC as $key => $value) {
							if(!empty($nameBCC[$key])){//add with name
								$mail->addBCC($value,$nameBCC[$key]);
								$example_code .= "\n\$mail->addBCC(\"$value\",\"{$nameBCC[$key]}\");";								
							}
							else{//address only
								$mail->addBCC($value);
								$example_code .= "\n\$mail->addBCC(\"$value\");";								
							}
						}
					}
					
					if (@$request['cc_Email'] != '') {
						$nameCC = [];
						if(@$request['cc_Name'] != ''){
							$nameCC = explode(" ", $request['cc_Name']);
						}
						$indiCC = explode(" ", $request['cc_Email']);
						foreach ($indiCC as $key => $value) {
							if(!empty($nameCC[$key])){//add with name
								$mail->addCC($value,$nameCC[$key]);
								$example_code .= "\n\$mail->addCC(\"$value\",\"{$nameCC[$key]}\");";								
							}
							else{//address only
								$mail->addCC($value);
								$example_code .= "\n\$mail->addCC(\"$value\");";								
							}
						}
					}
					
					if (@$request['Reply_To'] != '') {
						$nameReply = [];
						if(@$request['Reply_Name'] != ''){
							$nameReply = explode(" ", $request['Reply_Name']);
						}
						$indiReply = explode(" ", $request['Reply_To']);
						foreach ($indiReply as $key => $value) {
							if(!empty($nameReply[$key])){//add with name
								$mail->addReplyTo($value,$nameReply[$key]);
								$example_code .= "\n\$mail->addReplyTo(\"$value\",\"{$nameReply[$key]}\");";								
							}
							else{//address only
								$mail->addReplyTo($value);
								$example_code .= "\n\$mail->addReplyTo(\"$value\");";								
							}
						}
					}
					
				} catch (phpmailerException $e) { //Catch all kinds of bad addressing
					throw new phpmailerException($e->getMessage());
				}
				$mail->Subject = $request['Subject'];
				$example_code .= "\n\$mail->Subject  = \"" . @$request['Subject'] .
					' (PHPMailer test using ' . strtoupper($request['test_type']) . ')";';

				//if ($request['Message'] == '') {
					//$body = file_get_contents('contents.html');
				//} else {
					$body = @$request['Message'];
				//}

				$example_code .= "\n\$body = <<<'EOT'\n" . htmlentities($body) . "\nEOT;";

				$mail->WordWrap = 78; // set word wrap to the RFC2822 limit
				$example_code .= "\n\$mail->WordWrap = 78;";
								
				if(!empty($body)){//html mode
					$mail->msgHTML($body, dirname(__FILE__), true); //Create message bodies and embed images
					$example_code .= "\n\$mail->msgHTML(\$body, dirname(__FILE__), true); //Create message bodies and embed images";					
					if(!empty($request['Text'])){
						$mail->AltBody = $request['Text'];
						$example_code .= "\n\$mail->AltBody = \"{$request['Text']}\"";
					}
				}else{//text mode
					$mail->isHTML(false);// Set email format to Text
					$txt = @$request['Text'];
					$mail->Body    = $txt;
					$mail->AltBody = $txt;
					$example_code .= "\$mail->isHTML(false);";
					$example_code .= "\$mail->Body(\"$txt\");";
					$example_code .= "\$mail->AltBody(\"$txt\");";
				}

				//add attachment
				if(!empty($files) && (is_object($files)||is_array($files))){
					foreach($files as $filename=>$source){//this is good enough, make function!
						try{//send what's there!
							$mail->addAttachment($source, $filename); // optional name
							$example_code .= "\n\$mail->addAttachment('$source', '$filename');  // optional name";
						}catch(Exception $e){}
					}
				}
				
				//$mail->addAttachment('images/phpmailer.png', 'phpmailer.png'); // optional name
				//$example_code .= "\n\$mail->addAttachment('images/phpmailer_mini.png'," .
					//"'phpmailer_mini.png');  // optional name";
				//$example_code .= "\n\$mail->addAttachment('images/phpmailer.png', 'phpmailer.png');  // optional name";

				$example_code .= "\n\ntry {";
				$example_code .= "\n  \$mail->send();";
				$example_code .= "\n  \$results_messages[] = \"Message has been sent using " .
					strtoupper($request['test_type']) . "\";";
				$example_code .= "\n}";
				$example_code .= "\ncatch (phpmailerException \$e) {";
				$example_code .= "\n  throw new phpmailerException('Unable to send to: ' . \$to. ': '.\$e->getMessage());";
				$example_code .= "\n}";

				try {
					$mail->send();
					$results_messages[] = "Message has been sent using " . strtoupper($request["test_type"]);
				} catch (phpmailerException $e) {
					throw new phpmailerException("Unable to send to: " . $to . ': ' . $e->getMessage());
				}
			} catch (phpmailerException $e) {
				$results_messages[] = $e->errorMessage();
				return $return_messages = [
					"error"=>1,
					"message"=>$results_messages
				];
			}
			//remove file(s)
			if($deleteFilesFlag==1 && !empty($files) && (is_object($files)||is_array($files))){
				foreach($files as $filename=>$source){//this is good enough, make function!
					try{unlink($source);}catch(Exception $e){}
				}
			}
			//success
			return $return_messages = [
				"error"=>0,
				"message"=>"success"
			];
		}catch(Exception $e){
			//some unknown error
		   $results_messages = $e->getMessage()." line: ".$e->getLine();
			/*print_r(json_encode([
				"error"=>1,
				"message"=>$results_messages
			]));*/
			return $return_messages = [
				"error"=>1,
				"message"=>$results_messages
			];
		}		
	}
}