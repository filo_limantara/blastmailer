<?php
require_once("MailQueueLib.php");
require_once("SimpleMailerLib.php");
require_once("EasyFileUploadHandle.php");

$username = @$_POST['api_user'];
$password = @$_POST['api_key'];
try{
	$mailQueue = new MailQueue($username,$password);
	if($mailQueue->isAuthenticated()==true){
		$config = $mailQueue->getConfig();
		
		//handle mail parameters
		$mail = array_filter([
			"To_Email"=> @$_POST['to'],
			"To_Name"=> @$_POST['toname'],
			"Subject"=> @$_POST['subject'],
			"Message"=> @$_POST['html'],
			"Text"=> @$_POST['text'],
			"From_Email"=> @$_POST['from'],
			"From_Name"=> @$_POST['fromname'],
			"Reply_To"=> @$_POST['replyto'],
			"Reply_Name"=> @$_POST['replyname'],
			"cc_Email"=> @$_POST['cc'],
			"cc_Name"=> @$_POST['ccname'],
			"bcc_Email"=> @$_POST['bcc'],
			"bcc_Name"=> @$_POST['bccname']
		]);
		$mail = array_merge($mail,$config);
		
		//handle_files
		$files = [];
		
		//please handle the files onto this directory
		$files = easyFileUploadHandle(['files'],dirname(__FILE__)."\\uploads\\");
		
		//handle priorities
		$priority = (isset($_POST['priority']))?$_POST['priority']:0;
		
		//handle valid until
		$validuntil = @$_POST['validuntil'];
		//$mailQueue->push($mail,$files,$priority,$validuntil);
		print_r(json_encode(SimpleMailerLib::send($mail,$files,$deleteFilesFlag=1)));
	}else{//throw error not authd
		if(empty($allConfig)){throw new Exception('invalid username and password');}
	}
}catch(Exception $e){
   $results_messages = $e->getMessage()." line: ".$e->getLine();
	print_r(json_encode([
		"error"=>1,
		"message"=>$results_messages
	]));
}
?>